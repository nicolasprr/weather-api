import { IWeather, IWeatherResponse } from "../models/weather";

export const parseWeather = (weather: IWeatherResponse): IWeather => ({
  environment: {
    feels: weather.main.feels_like,
    humidity: weather.main.humidity,
    temperature: weather.main.temp,
  },
  sky: {
    description: weather.weather[0]?.description,
    icon: weather.weather[0]?.icon,
    type: weather.weather[0]?.main,
  },
  wind: {
    speed: weather.wind.speed,
  },
});
