import { IPlaceResponse, IPlace } from "../models/place";

export const parsePlaces = (places: IPlaceResponse[]): IPlace[] => {
  const countries: Record<string, IPlaceResponse> = {};

  places.forEach((place) => {
    countries[place.country] = place;
  });

  return Object.entries(countries).map(([key, place]) => ({
    name: place.name,
    country: place.country,
    enLocalName: place.local_names?.en,
    lat: place.lat,
    lng: place.lon,
  }));
};
