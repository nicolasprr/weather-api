import { createGlobalStyle } from "styled-components";

const baseWidth = 8;
export const theme = {
  fontSource:
    "https://fonts.googleapis.com/css2?family=Rubik:wght@300&display=swap",
  fontFamily: "Rubik', sans-serif",
  shadows: {
    general: "1px 1px 15px rgba(0, 0, 0, 0.1)",
  },
  colors: {
    light: {
      primary: "#0072c6",
      secondary: "#00b0f0",
      third: "#184E77",
      success: "#1EB73B",
      warning: "#FFC107",
      warning2: "#FFBA08",
      error: "#FF1744",
      error2: "#d00000",
      darkBlue: "#6E85B2",
      dark: "#111111",
    },
    gray: {
      primary: "#F9FAFB",
      secondary: "#6B7280",
    },
    text: {
      primary: "#111827",
    },
  },
  baseWidth: 8,
  fontSize: {
    lg: "1.1rem",
    xl: "1.3rem",
    md: "0.9rem",
    sm: "0.8rem",
  },
  sizes: {
    svg: {
      default: {
        minHeight: "46px",
      },
      small: {
        minHeight: "30px",
      },
      large: {
        minHeight: "55px",
      },
    },
    button: {
      default: {
        fontSize: "1rem",
        padding: {
          left: baseWidth * 2,
          right: baseWidth * 2,
          top: baseWidth * 2 - 2,
          bottom: baseWidth * 2 - 2,
        },
      },
      small: {
        fontSize: "0.8125rem",
        padding: {
          left: baseWidth * 0.9,
          right: baseWidth * 0.9,
          top: baseWidth * 0.9 - 2,
          bottom: baseWidth * 0.9 - 2,
        },
      },
      large: {
        fontSize: "1.1875rem",
        padding: {
          left: baseWidth * 2.5,
          right: baseWidth * 2.5,
          top: baseWidth * 2.5 - 2,
          bottom: baseWidth * 2.5 - 2,
        },
      },
    },
    tag: {
      default: {
        fontSize: "1rem",
        padding: {
          left: baseWidth * 1.3,
          right: baseWidth * 1.3,
          top: 3,
          bottom: 3,
        },
      },
      small: {
        fontSize: "0.8125rem",
        padding: {
          left: baseWidth * 0.9,
          right: baseWidth * 0.9,
          top: 2,
          bottom: 2,
        },
      },
      large: {
        fontSize: "1.1875rem",
        padding: {
          left: baseWidth * 1.7,
          right: baseWidth * 1.7,
          top: 3.2,
          bottom: 3.2,
        },
      },
    },

    lg: baseWidth * 6,
    xl: baseWidth * 5,
    md: baseWidth * 3,
    sm: baseWidth * 3,
  },
};

export const GlobalStyles = createGlobalStyle`
      @import url('https://fonts.googleapis.com/css2?family=Rubik:wght@300&display=swap'); 
      body {
      max-width: 100vw;
      font-family: 'Rubik', sans-serif;
      margin: 0;
    }
    
`;
