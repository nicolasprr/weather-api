//@ts-nocheck
import FramerButton from "../atoms/FramerButton";

const ButtonGroup = ({ value, onChange, options }) => (
  <>
    {options.map((item) => (
      <FramerButton
        key={item.value}
        size="small"
        onClick={() => onChange(item.value)}
        variant={value === item?.value && "selected"}
      >
        {item.label}
      </FramerButton>
    ))}
  </>
);
export default ButtonGroup;
