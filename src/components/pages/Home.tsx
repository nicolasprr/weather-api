import { useState } from "react";

import { Search } from "@styled-icons/material-outlined";
import FramerInput from "../atoms/FramerInput";
import FramerTitle from "../atoms/FramerTitle";
import Center from "../atoms/layout/Center";
import MotionCard from "../atoms/layout/MotionCard";
import Svg from "../atoms/Svg";
import { usePlaceMutation } from "../../services/usePlace";
import { IPlace } from "../../models/place";
import { parsePlaces } from "../../utils/places";
import ResultCity from "../atoms/ResultCity";
import { useWeatherMutation } from "../../services/weather";
import { IWeather, Weather } from "../../models/weather";
import ResultWeather from "../atoms/ResultWeather";
import { parseWeather } from "../../utils/weather";
import { AnimatePresence } from "framer-motion";

const Home = () => {
  const [inputValue, setInputValue] = useState("");

  const [places, setPlaces] = useState<IPlace[]>([]);
  const [currentPlace, setCurrentPlace] = useState<IPlace | null>(null);

  const [currentWeather, setCurrentWeather] = useState<IWeather | null>(null);

  const { mutateAsync, data } = usePlaceMutation();
  const { mutateAsync: mutateWeather } = useWeatherMutation();

  const handleSearch = async () => {
    if (!inputValue) return;
    setCurrentPlace(null);
    setCurrentWeather(null);
    setPlaces([]);

    const response = await mutateAsync(inputValue);
    const parsedPlaces = parsePlaces(response.data);
    setPlaces(parsedPlaces);
  };

  const handleWeather = async (place: IPlace) => {
    setCurrentPlace(place);
    setPlaces([place]);
    const response = await mutateWeather({ lat: place.lat, lng: place.lng });
    const parsedWeather = parseWeather(response.data);
    setCurrentWeather(parsedWeather);
  };

  return (
    <Center
      initial={{
        marginTop: "10%",
      }}
      animate={{
        marginTop: places.length >= 3 ? "5%" : "10%",
      }}
    >
      <FramerTitle> Select your city</FramerTitle>
      <div
        style={{
          display: "flex",
          width: "100%",
        }}
      >
        <FramerInput
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
          label="City"
        />
        <Svg
          onClick={handleSearch}
          clickeable
          color="secondary"
          direction="right"
        >
          <Search />
        </Svg>
      </div>
      <AnimatePresence>
        {places?.length > 0 && (
          <MotionCard
            whileHover={{
              scale: 1.01,
            }}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
          >
            {places.map((place) => (
              <ResultCity
                key={place.country}
                onClick={handleWeather}
                place={place}
              />
            ))}
          </MotionCard>
        )}
      </AnimatePresence>
      {currentWeather && (
        <ResultWeather currentPlace={currentPlace} weather={currentWeather} />
      )}
    </Center>
  );
};
export default Home;
