import { ComponentProps, useMemo } from "react";
import styled, { useTheme } from "styled-components";
import { motion } from "framer-motion";

import { theme } from "../../theme";

type ButtonSizeTypes = keyof typeof theme.sizes.button;

interface ButtonProps {
  active?: boolean;
  fullwidth: boolean;
  theme: typeof theme;
  size: ButtonSizeTypes;
  mainColor: string;
}

const Button = styled(motion.button)<ButtonProps>`
  width: ${({ fullwidth }) => (fullwidth ? "100%" : "auto")};
  color: white;
  padding-left: ${({ theme, size }) =>
    (size && theme.sizes.button[size].padding.left) ||
    theme.sizes.button.default.padding.left}px;

  padding-right: ${({ theme, size }) =>
    (size && theme.sizes.button[size].padding.right) ||
    theme.sizes.button.default.padding.right}px;
  padding-bottom: ${({ theme, size }) =>
    (size && theme.sizes.button[size].padding.bottom) ||
    theme.sizes.button.default.padding.bottom}px;
  padding-top: ${({ theme, size }) =>
    (size && theme.sizes.button[size].padding.top) ||
    theme.sizes.button.default.padding.top}px;
  font-size: ${({ theme, size }) =>
    (size && theme.sizes.button[size].fontSize) ||
    theme.sizes.button.default.fontSize};
  font-weight: 500;
  border: 2px solid
    ${(props) => props.mainColor || props.theme.colors.light.primary};
  background: ${(props) => props.mainColor || props.theme.colors.light.primary};
  cursor: ${({ active, disabled }) =>
    active ? "default" : (disabled && "not-allowed") || "pointer"};
  :disabled {
    cursor: unset;
    -o-transition-property: none !important;
    -moz-transition-property: none !important;
    -ms-transition-property: none !important;
    -webkit-transition-property: none !important;
    transition-property: none !important;
    -o-transform: none !important;
    -moz-transform: none !important;
    -ms-transform: none !important;
    -webkit-transform: none !important;
    transform: none !important;
    -moz-animation: none !important;
    -o-animation: none !important;
    -ms-animation: none !important;
    animation: none !important;
  }
`;

interface Props extends ComponentProps<typeof Button> {
  size: ButtonSizeTypes;
  variant: "default" | "selected" | "warning";
}

const FramerButton = ({
  children,
  variant = "default",
  size,
  ...props
}: Props) => {
  const theme = useTheme();
  const variants = {
    default: {
      main: theme.colors.light.primary,
      hover: theme.colors.light.secondary,
      tap: theme.colors.light.third,
    },
    selected: {
      main: theme.colors.light.secondary,
      hover: theme.colors.light.primary,
      tap: theme.colors.light.primary,
    },
    warning: {
      main: theme.colors.light.warning,
      hover: theme.colors.light.warning2,
      tap: theme.colors.light.warning2,
    },
    error: {
      main: theme.colors.light.error,
      hover: theme.colors.light.error2,
      tap: theme.colors.light.error2,
    },
  };
  const currentColor = useMemo(
    () => variants[variant] ?? variants.default,
    [variant]
  );

  return (
    <Button
      
      size={size}
      mainColor={currentColor?.main}
      animate={{
        backgroundColor: currentColor?.main,
        borderColor: currentColor?.main,
      }}
      whileHover={{
        scale: 1.034,
        backgroundColor: currentColor?.hover,
        borderColor: currentColor?.hover,
        textShadow: "0px 0px 8px rgb(255, 255, 255)",
      }}
      whileTap={{
        scale: 0.9,
        backgroundColor: currentColor?.tap,
        borderColor: currentColor?.tap,
        boxShadow: "0px 0px 8px #6E85B2",
      }}
      {...props}
    >
      {children}
    </Button>
  );
};

export default FramerButton;
