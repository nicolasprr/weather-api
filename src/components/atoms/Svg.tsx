import { ReactNode } from "react";
import styled from "styled-components";
import { HTMLMotionProps, motion } from "framer-motion";
import { theme } from "../../theme";

interface ContainerProps {
  clickeable: boolean;
  direction: "left" | "right" | null;
  size?: keyof typeof theme.sizes.svg;
}

const SvgContainer = styled(motion.div)<ContainerProps>`
  justify-content: center;
  cursor: ${({ clickeable }) => (clickeable ? "pointer" : "default")};
  min-height: ${({ theme, size = "default" }) =>
    theme.sizes.svg[size]?.minHeight};
  width: 44px;
  box-shadow: 1px 1px 15px rgba(0, 0, 0, 0.1);
  align-self: flex-end;
  height: 100%;
  max-height: 48px;
  display: flex;
  border-bottom-left-radius: ${({ direction }) =>
    direction === "left" ? 10 : 0}px;
  border-top-left-radius: ${({ direction }) =>
    direction === "left" ? 10 : 0}px;
  border-bottom-right-radius: ${({ direction }) =>
    direction === "right" ? 10 : 0}px;
  border-top-right-radius: ${({ direction }) =>
    direction === "right" ? 10 : 0}px;
`;

interface ConfigProps {
  color: keyof typeof theme.colors.light;
}

const SvgConfig = styled.div<ConfigProps>`
  align-self: center;
  & > svg {
    height: 30px;
    color: ${({ theme, color }) =>
      color ? theme.colors.light[color] : theme.colors.light.primary};
  }
`;

interface SvgProps extends ConfigProps, ContainerProps {
  children: ReactNode;
  onClick?: () => void;
  containerProps?: HTMLMotionProps<"div">;
}

const Svg = ({
  children,
  clickeable,
  color,
  direction,
  onClick,
  containerProps,
  size,
}: SvgProps) => (
  <SvgContainer
    size={size}
    clickeable={clickeable}
    direction={direction}
    onClick={onClick}
    whileTap={{ scale: 0.8 }}
    {...containerProps}
  >
    <SvgConfig color={color}>{children}</SvgConfig>
  </SvgContainer>
);

export default Svg;
