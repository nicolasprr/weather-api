import { motion } from "framer-motion";
import styled from "styled-components";

const Center = styled(motion.div)`
  flex-direction: column;
  display: flex;
  align-items: center;
  margin-left: auto;
  margin-right: auto;
  justify-content: center;
  margin-top: 10%;
  margin-left: 30%;
  margin-right: 30%;
`;

export default Center;
