import { motion } from "framer-motion";
import styled from "styled-components";

const MotionCard = styled(motion.div)`
  margin-top: 10px;
  box-sizing: border-box;
  border-radius: 10px;
  width: 100%;
  height: fit-content;
  padding: 8px 16px;
  box-shadow: ${({ theme }) => theme.shadows.general};
`;

export default MotionCard;
