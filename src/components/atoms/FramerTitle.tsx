import { ReactNode } from "react";
import styled from "styled-components";
import { motion } from "framer-motion";

const Title = styled(motion.h1)`
  /* text-shadow: 0px 0px 8px #368b85; */
  font-size: ${(props) => props.theme.fontSize.xl};
  @media screen and (max-width: 768px) {
    font-size: ${(props) => props.theme.fontSize.md};
  }
`;

interface Props {
  children: ReactNode;
}

const FramerTitle = ({ children }: Props) => (
  <Title initial={{ x: -110 }} animate={{ x: 0 }}>
    {children}
  </Title>
);

export default FramerTitle;
