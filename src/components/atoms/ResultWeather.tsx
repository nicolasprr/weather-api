import { useState } from "react";

import { IPlace } from "../../models/place";
import { IWeather } from "../../models/weather";
import ButtonGroup from "../organims/ButtonGroup";
import FramerTitle from "./FramerTitle";
import MotionCard from "./layout/MotionCard";

interface Props {
  weather: IWeather;
  currentPlace?: IPlace | null;
}

const ResultWeather = ({ weather, currentPlace }: Props) => {
  const [measure, setMeasure] = useState<"F" | "D">("F");

  const measureString = measure === "F" ? "°F" : "°C";

  const feels = weather.environment.feels;

  const finalTemp =
    measure === "F" ? feels : (((feels - 32) * 5) / 9).toFixed(2);

  return (
    <MotionCard
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      <ButtonGroup
        options={[
          { value: "F", label: "Farenheit" },
          { value: "D", label: "Degree" },
        ]}
        value={measure}
        onChange={(e: "F" | "D") => setMeasure(e)}
      />
      <FramerTitle>{currentPlace?.enLocalName}</FramerTitle>
      <p>Type: {weather?.sky?.type}</p>
      <p>{weather?.sky?.description}</p>
      <img
        src={`http://openweathermap.org/img/wn/${weather?.sky?.icon}@2x.png`}
      />
      <p>wind speed: {weather?.wind.speed} miles/hour</p>
      <p>
        temp: {finalTemp} {measureString}
      </p>
    </MotionCard>
  );
};

export default ResultWeather;
