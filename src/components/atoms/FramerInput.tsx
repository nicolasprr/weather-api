import { useState, ComponentProps, EventHandler, ChangeEvent } from "react";
import styled, { useTheme } from "styled-components";
import {
  motion,
  AnimatePresence,
  MotionProps,
  HTMLMotionProps,
} from "framer-motion";

import Label from "./Label";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;
const Container = styled(motion.div)`
  box-sizing: border-box;
  max-width: 100%;
  padding: 1px 4px;
  border-radius: 5px;
  align-items: center;
  width: 100%;
  border: 1px solid rgb(234, 236, 239);
  display: flex;
  justify-content: space-between;
  box-shadow: ${({ theme }) => theme.shadows.general};
  height: 46px;
`;

const Input = styled(motion.input)`
  background-color: inherit;
  width: 100%;
  max-width: 100%;
  height: 40px;
  border: none;
  font-size: 1.1rem;
  :focus {
    outline: none;
  }
`;

interface Props extends ComponentProps<typeof Input> {
  label: string;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

const FramerInput = ({ label, ...rest }: Props) => {
  const [focus, setFocus] = useState(false);
  const theme = useTheme();

  return (
    <Wrapper>
      <AnimatePresence>
        {label && (
          <Label
            initial={{
              opacity: 0,
            }}
            exit={{
              opacity: 0,
            }}
            animate={{
              opacity: 1,
            }}
            transition={{
              opacity: {
                delay: 0.09,
              },
            }}
          >
            {label}
          </Label>
        )}
      </AnimatePresence>

      <Container
        animate={{
          borderColor: focus
            ? theme.colors.light.primary
            : "rgb(234, 236, 239)",
        }}
        transition={{
          duration: 0.2,
        }}
        whileHover={{
          borderColor: theme.colors.light.primary,
        }}
      >
        <Input
          onBlur={() => setFocus(false)}
          onFocus={() => setFocus(true)}
          whileFocus={{
            x: 5,
          }}
          {...rest}
        />
      </Container>
    </Wrapper>
  );
};

export default FramerInput;
