import styled from "styled-components";
import { motion } from "framer-motion";

const Label = styled(motion.label)`
  /* font-size: 0.6rem; */
  /* margin-bottom: 6px; */
  font-size: 0.9rem;
  font-weight: bold;
  display: flex;
  width: 100%;
  align-items: center;
  line-height: 26px;
  color: rgb(30, 35, 41);
`;

export default Label;
