import { RemoveRedEye } from "@styled-icons/material-outlined";

import { IPlace } from "../../models/place";
import FramerTitle from "./FramerTitle";
import Svg from "./Svg";

interface Props {
  place: IPlace;
  onClick: (place: IPlace) => void;
}

const ResultCity = ({ place, onClick }: Props) => {

  return (
    <>
      <FramerTitle>
        {place.name} ({place.country})
      </FramerTitle>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          width: "100%",
        }}
      >
        <p>
          <span>Longitude: {place.lng.toFixed(3)},</span>
          <span> Latitude: {place.lat.toFixed(3)}</span>
        </p>
        <Svg
          onClick={() => onClick(place)}
          size="small"
          containerProps={{
            style: {
              height: "100%",
              margin: "auto -16px auto auto",
            },
          }}
          clickeable
          color="primary"
          direction="left"
        >
          <RemoveRedEye />
        </Svg>
      </div>
    </>
  );
};

export default ResultCity;
