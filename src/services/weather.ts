import { useMutation } from "@tanstack/react-query";
import instance from "./axios";

interface IVariables {
  lat: number;
  lng: number;
}

export const useWeatherMutation = () =>
  useMutation({
    mutationKey: ["weather"],
    mutationFn: (variables: IVariables) =>
      getWeather(variables.lat, variables.lng),
  });

const url = "data/2.5/weather/";

const getWeather = (lat: number, lng: number) => {
  return instance.get(url, {
    params: {
      lat,
      lon: lng,
      units: "imperial",
    },
  });
};
