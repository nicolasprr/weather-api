import { AxiosResponse } from "axios";
import { IPlaceResponse } from "../models/place";
import instance from "./axios";

const url = "geo/1.0/direct";

export const getPlaces = async (cityName: string) => {
  const response = await instance.get<any, AxiosResponse<IPlaceResponse[]>>(
    url,
    {
      params: {
        q: cityName,
      },
    }
  );
  return response
};
