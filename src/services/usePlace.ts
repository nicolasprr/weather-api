
import {useMutation} from "@tanstack/react-query"
import { getPlaces } from "./place"


export const usePlaceMutation = () => useMutation({
    mutationKey: ["places"],
    mutationFn: (fileName: string) => getPlaces(fileName)
})