import axios from "axios";

const defaultUrl = "https://api.openweathermap.org/";
const appId = "96c880d541e43bba7a6a6eca9e88c799";

const instance = axios.create({
  baseURL: defaultUrl,
  params: {
    appid: appId,
    limit: 5,
  },
});


export default instance